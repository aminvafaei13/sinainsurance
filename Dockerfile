FROM node:12.14-alpine

WORKDIR /app

COPY entrypoint.sh /entrypoint.sh

RUN npm i -g pm2 && \
  npm i -g @adonisjs/cli && \
  chown node:node /app && \
  chmod 755 /entrypoint.sh

USER node

ADD --chown=node:node package.json package-lock.json ./

RUN npm ci --production

COPY --chown=node:node . .

RUN mkdir -p /app/resources/uploads && chown node:node /app/resources/uploads

ENV NODE_ENV=development HOST=0.0.0.0 PORT=3000 ENV_SILENT=true


ENTRYPOINT [ ".build/entrypoint.sh" ]
# CMD [ "adonis", "serve", "--dev" ]