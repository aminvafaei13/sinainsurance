const { ServiceProvider } = require('@adonisjs/fold')

/**
 * Register node-forge package as service provider
 */
class NodeForgeProvider extends ServiceProvider {
  register () {
    this.app.singleton('Sina/NodeForge', () => {
      return new (require('.'))()
    })
  }
}

module.exports = NodeForgeProvider
