'use strict'

const _nodeForge = require('node-forge')

class NodeForge {
  constructor() {
    return _nodeForge;
  }
}

module.exports = NodeForge
