"use strict"

const soap = require("soap")
const forge = use("Sina/NodeForge")

class Soap {
  constructor(Config) {
    this.Config = Config
    this.soap = soap

    // let environment = this.Config.get("sina.environment", "production")

    // this.url = this.Config.get(`sina.${environment}.url`, "")

    // this.pin_key = this.Config.get(`sina.${environment}.pin_key`, "")
    // this.mac_key = this.Config.get(`sina.${environment}.mac_key`, "")
    // this.master = this.Config.get(`sina.${environment}.master`, "")

    // this.username = this.Config.get(`sina.${environment}.user`, "")
    // let password = forge.md.sha1.create()
    // password.update(this.Config.get(`sina.${environment}.password`, ""))
    // password = password.digest().toHex()
    // console.log(password)
    // this.password = Soap.encrypt3DES(password, this.pin_key)

    // this.card_acceptor_code = this.Config.get(`sina.${environment}.card_acceptor_code`, "")
    // this.device_code = this.Config.get(`sina.${environment}.device_code`, "")
  }

  static encrypt3DES (input, key) {
    var cipher = forge.cipher.createCipher('3DES-ECB', key.substring(0, 24))
    cipher.start()
    cipher.update(forge.util.createBuffer(Buffer.from(input, "utf8").toString("binary")))
    cipher.finish()
    var encrypted = cipher.output

    return Buffer.from(encrypted.getBytes(), "binary").toString("hex")
  }

  async createClient (url, options = {}) {
    console.log(options)
    return await this.soap.createClientAsync(url, options)
  }
}

module.exports = Soap