"use strict"

const { ServiceProvider } = require("@adonisjs/fold")

class SoapProvider extends ServiceProvider {
  register () {
    this.app.singleton('Sina/Soap', () => {
      const Config = use("Config")
      return new (require('.'))(Config)
    })
  }
}

module.exports = SoapProvider