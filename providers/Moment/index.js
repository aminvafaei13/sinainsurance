"use strict"

const moment = require("moment");

class Moment {
    constructor() {
        return moment;
    }
}

module.exports = Moment;