"use strict"

const { ServiceProvider } = require("@adonisjs/fold")

class MomentProvider extends ServiceProvider {
  register () {
    this.app.singleton('Sina/Moment', () => {
      return new (require('.'))()
    })
  }
}

module.exports = MomentProvider