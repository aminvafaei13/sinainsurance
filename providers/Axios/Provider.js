"use strict"

const { ServiceProvider } = require("@adonisjs/fold")

class AxiosProvider extends ServiceProvider {
  register () {
    this.app.singleton('Sina/Axios', () => {
      const Config = use("Config")
      return new (require('.'))(Config)
    })
  }
}

module.exports = AxiosProvider