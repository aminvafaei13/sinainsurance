"use strict"

const Drive = use("Drive")
const axios = require("axios")

class Axios {
    constructor(Config) {
        this.Config = Config;
    }

    async twoWay() {
        const cert = await Drive.get(this.Config.get("axios.twoWay.cert")),
            key = await Drive.get(this.Config.get("axios.twoWay.cert")),
            passphrase = this.Config.get("axios.twoWay.passphrase"),
            httpsAgent = {
                cert: cert,
                key: key,
                passphrase: passphrase
            }

        return axios.create({
            httpsAgent
        });

    }

    normal() {
        return axios.create();
    }
}

module.exports = Axios;