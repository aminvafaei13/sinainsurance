"use strict"

const { ServiceProvider } = require("@adonisjs/fold")

class XmlProvider extends ServiceProvider {
  register () {
    this.app.singleton('Sina/Xml', () => {
      return new (require('.'))()
    })
  }
}

module.exports = XmlProvider