#!/bin/sh
set -e

 

if [ "$1" == "rest" ]
then 
     echo ${NODE_ENV}
    if [ "${NODE_ENV}" == "development" ]
    then
      echo "Development PM2"
      exec pm2-runtime start dev-process.json
    else    
      echo "Production PM2"
      exec pm2-runtime start prod-process.json    
    fi
else
    exec $@
fi
