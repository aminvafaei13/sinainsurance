module.exports = {
  HttpResponse: {
    ok: 200,
    validation_fail: 400,
    auth_failed: 401,
    set_config: 402,
    forbidden: 403,
    not_found: 404,
    conflict: 409,
    not_processable: 422,
    not_response: 500
  },
  /**
   * add custom roles in here to use them with the RoleSeeder
   */
  roles: {
    MASTER_BANK_ADMIN: 'master_bank_admin',
    BANK_ADMIN: 'bank_admin',
    OPERATOR: 'operator',
    USER: 'user'
  }
}
