"use strict"

const Antl = use("Antl");
const Soap = use("Maskan/Soap");
const moment = use("Maskan/Moment");
const forge = use("Maskan/NodeForge");
// const Client = await Soap.createClient();

/**
 * Check if the received homa date is equal to our server and if failed
 * return the error
 * 
 * @param {string} receivedDate "123456#20200317125623"
 */
function checkServerDate(receivedDate) {
    if (receivedDate != moment().format("YYYYMMDDHHmmss")) {
        throw new Error(Antl.formatMessage("validation.server_date_invalid"))
    }
}

/**
 * Get a server seed to fetch session id
 */
async function getServerSeed() {
    // TODO: get results from homa switch
    //let result = await Client.GetSeed(Soap.username, Soap.password);


    let result = {
        StringValue: `${getClientSeed()}#${moment().format("YYYYMMDDHHmmss")}`,
        ResponseCode: "000",
        Description: "Description"
    }
    // Check if our server date is matching the response
    checkServerDate(result.StringValue.split("#")[1])

    // Return the server seed
    return result.StringValue.split("#")[0];
}

function getRandomSixDigit(){
    return getClientSeed();
}

function getClientSeed() {
    return `${Math.floor(Math.random() * (999999 - 100000)) + 100000}`;
}

/**
 * get session if from service
 */
async function getSessionId() {
    let clientSeed = await getClientSeed(),
        serverSeed = await getServerSeed();

    // TODO: get results from homa switch
    // let result = await Client.GetSessionId(
    //     Soap.username,
    //     Crypto.hashString(`${clientSeed}#${Crypto.shaHash(`${clientSeed}#${serverSeed}`)}#${Crypto.shaHash(Soap.username)}#${moment().format("YYYYMMDDHHmmss")}`, Soap.master)
    // );

    let result = {
        StringValue: getClientSeed(),
        ResponseCode: "000",
        Description: ""
    };
    return result.StringValue;
}

/**
 * Encrypt by 3DES using node-forge
 * @param input utf8 string
 * @param key key is shorten to maximum 192 bits
 * @returns {String} Output is a base64 string
 */
function encrypt3DES(input, key) {
    var cipher = forge.cipher.createCipher('3DES-ECB', key.substring(0, 24));
    cipher.start();
    cipher.update(forge.util.createBuffer(Buffer.from(input, "utf8").toString("binary")));
    cipher.finish();
    var encrypted = cipher.output;

    return Buffer.from(encrypted.getBytes(), "binary").toString("hex")
}

/**
 * Decrypt by 3DES using node-forge
 * @param input A base64 sring
 * @param key key is shorten to maximum 192 bits
 * @returns {String} A utf8 string
 */
function decrypt3DES(input, key) {
    const decipher = forge.cipher.createDecipher('3DES-ECB', key.substring(0, 24));
    decipher.start();

    const inputEx = forge.util.createBuffer(Buffer.from(input, "hex").toString("binary"));
    decipher.update(inputEx);
    decipher.finish();
    const decrypted = decipher.output;
    return Buffer.from(decrypted.getBytes(), "binary").toString("utf8")
}

/**
 * Encrypt by 3DES using node-forge
 * @param input A base64 sring
 * @param key key is shorten to maximum 192 bits
 * @param IV key is shorten to maximum 192 bits
 * @returns {String} A utf8 string
 */
function encryptDES(input, key, iv) {
    // encrypt some bytes
    var cipher = forge.rc2.createEncryptionCipher(key);
    cipher.start(iv);
    cipher.update(forge.util.createBuffer(input));
    cipher.finish();
    var encrypted = cipher.output;

    // outputs encrypted hex
    return encrypted.toHex();
}

/**
 * Decrypt by 3DES using node-forge
 * @param encrypted A base64 sring
 * @param key key is shorten to maximum 192 bits
 * @param IV key is shorten to maximum 192 bits
 * @returns {String} A utf8 string
 */
function decryptDES(encrypted, key, iv) {
    // decrypt some bytes
    var cipher = forge.rc2.createDecryptionCipher(key);
    cipher.start(iv);
    cipher.update(encrypted);
    cipher.finish();

    // outputs decrypted hex
    return cipher.output.toHex()
}

async function generateMAC(args = {}) {
    let sessionId = await getSessionId(),
        mac_key = Soap.mac_key.match(/.{1,16}/g),
        MAC = "",
        iv = "00000000",
        TEMP_MAC_1,
        TEMP_MAC_2,
        TEMP_MAC_3;

    /**
     * Sort all args based on their keys alphabetically and append to MAC
     */
    Object.keys(args).sort().forEach((key) => {
        MAC = `${MAX}${args[key].toString()}`
    })

    MAC = `${MAC}${sessionId}`

    while (MAC.length % 8 > 0) {
        MAC = `${MAC}0`
    }

    MAC.match(/.{1,8}/g).map((part) => {
        iv = encryptDES(part, mac_key[0], iv);
    })

    TEMP_MAC_1 = iv;
    TEMP_MAC_2 = decryptDES(TEMP_MAC_1, mac_key[3], iv);
    TEMP_MAC_3 = encryptDES(TEMP_MAC_2, mac_key[0], iv);

    return Buffer.from(TEMP_MAC_3).toString("hex");
}

module.exports = {
    checkServerDate: checkServerDate,
    getClientSeed: getClientSeed,
    getRandomSixDigit: getRandomSixDigit,
    getServerSeed: getServerSeed,
    getSessionId: getSessionId,
    encrypt3DES: encrypt3DES,
    decrypt3DES: decrypt3DES,
    encryptDES: encryptDES,
    decryptDES: decryptDES,
    generateMAC: generateMAC,
}