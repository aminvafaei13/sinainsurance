'use strict'

/**
 * The base service class that initiate all the properties that needed
 * for it's children
 */
class BaseService {
  constructor() {
    this.env = use('Env')
    this.dev_mode = this.env.get("NODE_ENV", "development") === "development"
    this.config = use('Config')
    this.exception = use('App/Exceptions/Exception')
  }

  /**
   * Get validation messages and make them as a single string
   *
   * @param {validation} validation
   */
  validationErrorMaker (validation) {
    // throw new Error(validation.messages().map(({ message }) => {
    //   return message
    // }).join(' '))
  }
}

module.exports = BaseService