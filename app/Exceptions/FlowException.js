class FlowException extends Error {
  constructor (message, status) {
    super(message)
    this.status = status || 422
  }
}

module.exports = FlowException
