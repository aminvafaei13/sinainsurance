/** @type {import('../../types').Config} */
const Config = use('Config')
/** @type {import('../../types').Logger} */

/** @type {import('../../types').BaseExceptionHandler} */
const BaseExceptionHandler = use('BaseExceptionHandler')

const IS_PROD = Config.get('app.appEnv') === 'production'

/**
 * This class handles all exceptions thrown during
 * the HTTP request lifecycle.
 *
 * @class ExceptionHandler
 */
class ExceptionHandler extends BaseExceptionHandler {
  /**
   * Handle exception thrown during the HTTP lifecycle
   *
   * @param {object} error
   * @param {CTX} ctx
   */
  async handle (error, { request, response }) {
    console.log(error)
    const errData = {
      status: error.status,
      result: error.messages,
      stack: error.stack
    }

    if (IS_PROD) {
      errData.stack = 'Sorry, there was a problem'
      if (error.status >= 500) {
        errData.result = 'Something went wrong'
      }
    }
    return response.status(error.status).json(errData)
  }
}

module.exports = ExceptionHandler
