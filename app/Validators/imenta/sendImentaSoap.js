'use strict'

const { rule } = use('Validator')

class sendImentaSoap {
  get rules () {
    return {
      Transid: 'required|string',
      mb: 'required|string',
      Tdate: 'required|string',
      ttime: 'required|string',
      Nc: 'required|string',
      Na: 'required|string',
      Lna: 'required|string',
      Fna: 'required|string',
      INo: 'required|number',
      BYear: 'required|number',
      BMonth: 'required|number',
      BDay: 'required|number',
      PCode: 'required|string',
      Tid: 'required|number',
      Prm: 'required|string',
      UoA: 'required|boolean|in:true,false',
      Jens: 'required|number|in:0,1',
      Mso: 'required|number',
      PolicyStartDate: 'required|string',
      CompanyCode: 'required|number'
    }
  }

  get validateAll () {
    return true
  }



}

module.exports = sendImentaSoap
