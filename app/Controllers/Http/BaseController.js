'use strict'

/**
 * Controller parent that initialize all the properties that they share
 * between each other
 */
class BaseController {
    constructor(fileName) {
        fileName = fileName.replace(/\\/g, '/') // replace back slashes with forward slash in case project is running on windows environment
        const service = use(`App/Services/${fileName.split('/')[fileName.split('/').length - 1].replace('Controller.js', 'Service')}`)
        this.service = new service()
        this.config = use('Config')
        this.exception = use('App/Exceptions/Exception')
        this.errorHandler = use('App/Utils/ErrorUtils').catchError
    }
}

module.exports = BaseController