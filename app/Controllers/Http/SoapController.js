'use strict'

const Antl = use("Antl")
const Drive = use("Drive")
const XML = use("Sina/Xml")
const Soap = use("Sina/Soap")
const moment = use("Sina/Moment")
const Config = use("Config")
const axios = require("axios")
const parser = require('fast-xml-parser')

class SoapController {
  /**
   * Post Send Imenta (fire) soap
   *
   * @param {request} param0
   */
  async sendImentaSoap ({ request, response }) {


    try {
      let imenta = Config.get("sina.imenta")
      const url = imenta.url
      const username = imenta.user
      const password = imenta.password

      var config = {
        headers: { 'Content-Type': 'text/xml' }
      }

      const data = request.only(['Transid', 'mb', 'Tdate', 'ttime', 'Nc', 'Na', 'Lna', 'Fna',
        'INo', 'BYear', 'BMonth', 'BDay', 'PCode', 'Tid', 'Prm', 'UoA', 'Jens', 'Mso',
        'PolicyStartDate', 'CompanyCode'])

      var xmlBodyStr = `<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope"
       xmlns:s="https://s-fan3.sina.local/">
          <soap:Header>
                <s:AuthHeader>
                    <s:Username>${username}</s:Username>
                    <s:Password>${password}</s:Password>        
                </s:AuthHeader>
          </soap:Header>
          <soap:Body>
                <s:APInsurancejsonInput>
                    <s:Input>
                    ${JSON.stringify(data)}
                    </s:Input>
                </s:APInsurancejsonInput>
          </soap:Body>
        </soap:Envelope>`
      const apiResponse = await axios.post(url, xmlBodyStr, config)

      var jsonObj = parser.parse(apiResponse.data, {})
      return response.sendResponse(200, jsonObj)
    } catch (error) {
      return response.sendResponse(400, error.message)

      console.error(error)
    }
  }


}

module.exports = SoapController
