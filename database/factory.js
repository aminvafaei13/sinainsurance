'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

function createValidNationalID() {
    let code = Math.floor(Math.random() * 999999989) + 100;

    let total = 0
    code.toString().split('').reverse().map((char, key) => {
        total += parseInt(char) * (key + 2)
    })

    let controller;
    if (total % 11 >= 2) {
        controller = parseInt(11 - (total % 11))
    }

    if (total % 11 < 2) {
        controller = total % 11
    }

    return unifyNationalId(`${code}${controller}`);
}

/**
 * Make sure all national_ids are following smae rule and all have length
 * of 10 cahracter
 *
 * @param {*} national_id
 */
function unifyNationalId(national_id = '') {
    national_id = national_id.toString()
    while (national_id.length !== 10) {
        national_id = `0${national_id}`
    }
    return national_id
}

function randomCompanyNames(key) {
    let names = {
        "Roshd-o-tose Sanat Pars": "رشد و توسعه صنعت پارس",
        "Refah Etesal Sepahan": "رفاه اتصال سپاهان",
        "Rafi Novin": "رفیع نوین",
        "Roxan Parsian": "رکسان پارسیان",
        "Rangan Far": "رنگان فر",
        "Rangin Profile Kavir": "رنگین پروفیل کویر",
        "Torico": "توریکو",
        "Iralco": "ایرالکو",
        "Rangin Nama Pouya": "رنگین نما پویا",
        "Rahavaran Sanat Bargh": "ره آوران صنعت برق",
        "Rahavard Saei": "ره آورد ساعی",
        "Rahpouyan Jebhe Sabz": "ره پویان جبهه سبز",
        "Rahpouyan Jonbesh Edalat Sabz": "ره پویان جنبش عدالت و توسعه",
        "Rahpouyan Pouyesh Bonyan": "ره پویان پویش بنیان",
        "Raha Choub": "رها چوب",
        "Rahavard Choub": "رهاورد چوب",
        "Rahavard Sarzamin Persia": "رهاورد سرزمین پرشیا",
        "Rahavard Sepehr Andishe": "رهاورد سپهر اندیشه",
        "Rahshad Electric": "رهشاد الکتریک"
    }

    return key === undefined ? names : names[key] === undefined ? names["Roshd-o-tose Sanat Pars"] : names[key]
}

function randomName(key) {
    let names = {
        Ali: "علی",
        Hossein: "حسین",
        Hasan: "حسن",
        Reza: "رضا",
        Kazem: "کاظم",
        Mahdi: "مهدی",
        Bagher: "باقر",
        Parmida: "پارمیدا",
        Elmira: "المیرا",
        Shaghayegh: "شقایق",
        Zahra: "زهرا",
        Nazanin: "نازنین"
    }

    return key === undefined ? names : names[key] === undefined ? names.Ali : names[key]
}

function randomFamilyName(key) {
    let names = {
        Safi: "صفی",
        Karbalaei: "کربلایی",
        Yaghoubi: "یعقوبی",
        Davoudi: "داودی",
        Behnia: "بهنیا",
        Anjavi: "انجوی",
        Mousavi: "موسوی",
        Chegini: "چگینی",
        Nekoupayan: "نکوپایان",
        Ebrahimi: "ابراهیمی",
        Vahdat: "وحدت",
        Jahantight: "جهان‌تیغ"
    }

    return key === undefined ? names : names[key] === undefined ? names.Safi : names[key]
}

function randomProvince(key) {
    let names = {
        "021": "تهران",
        "041": "آذربایجان شرقی",
        "044": "آذربایجان غربی",
        "0841": "ایلام",
        "045": "اردبیل",
        "031": "اصفهان",
        "077": "بوشهر",
        "051": "خراسان رضوی",
    }

    return key === undefined ? names : names[key] === undefined ? names['021'] : names[key]
}

function generateStockHolder(count, faker) {
    let stockHolders = []

    for (let i = 1; i < count + 1; i++) {
        stockHolders.push({
            PersonType: faker.pickone(['P', 'C']),
            CustomerCode: faker.integer({ min: 10000000, max: 99999999 }),
            PersonName: faker.pickone(Object.values(randomName())),
            SharePercentage: Math.floor(100 / count)
        })
    }

    return stockHolders
}

function generateManager(count, faker) {
    let managers = []

    for (let i = 1; i < count + 1; i++) {
        managers.push({
            CustomerCode: faker.integer({ min: 10000000, max: 99999999 }),
            PersonName: faker.pickone(Object.values(randomName())),
            Position: faker.pickone(['D', 'B', 'M', 'O']),
            ExpirationDate: `${faker.integer({ min: 1400, max: 1420 })}/${faker.integer({ min: 1, max: 12 })}/${faker.integer({ min: 1, max: 29 })}`
        })
    }

    return managers
}

function generateSignators(count, faker) {
    let signators = []

    for (let i = 1; i < count + 1; i++) {
        signators.push({
            CustomerCode: faker.integer({ min: 10000000, max: 99999999 }),
            PersonName: faker.pickone(Object.values(randomName())),
            ExpirationDate: `${faker.integer({ min: 1400, max: 1420 })}/${faker.integer({ min: 1, max: 12 })}/${faker.integer({ min: 1, max: 29 })}`
        })
    }

    return signators
}

function generateAddresses(count, faker) {
    let addresses = []

    for (let i = 1; i < count + 1; i++) {
        let ProvinceCode = faker.pickone(Object.keys(randomProvince()))
        let CityCode = faker.pickone(Object.keys(randomProvince()))
        addresses.push({
            AddressType: faker.pickone(['H', 'W', 'C']),
            PostalCode: faker.integer({ min: 1000000000, max: 9999999999 }),
            ProvinceCode: ProvinceCode,
            ProvinceName: randomProvince(ProvinceCode),
            CityCode: CityCode,
            CityName: randomProvince(CityCode),
            Address1: faker.paragraph(),
            Address2: faker.paragraph(),
            Address3: faker.paragraph(),
            PhoneNumber1: `${CityCode}${faker.integer({ min: 10000000, max: 99999999 })}`,
            PhoneNumber2: `${CityCode}${faker.integer({ min: 10000000, max: 99999999 })}`,
            FaxNumber: `${CityCode}${faker.integer({ min: 10000000, max: 99999999 })}`
        })
    }

    return addresses
}

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

Factory.blueprint('App/Models/FakeLegalEntity', (faker, i, data) => {
    let CompanyName = faker.pickone(Object.keys(randomCompanyNames()))
    let AuditorName = faker.pickone(Object.keys(randomName()))
    return {
        CustomerCode: data.CustomerCode ? data.CustomerCode : faker.integer({ min: 1000, max: 9999 }),
        Nationality: faker.pickone(["i", "F"]),
        ClientGroupCode: faker.integer({ min: 1, max: 10 }),
        ClientGroupDesc: faker.paragraph(),
        UnderStablishment: faker.pickone(['Y', 'N']),
        ForeignIdentityNo: createValidNationalID(),
        CompanyIdentityNo: faker.integer({ min: 10000000000, max: 19999999999 }),
        CompanyName: randomCompanyNames(CompanyName),
        RegistrationNumber: faker.integer({ min: 100000, max: 999999 }),
        RegistrationDate: `${faker.integer({ min: 1300, max: 1398 })}/${faker.integer({ min: 1, max: 12 })}/${faker.integer({ min: 1, max: 29 })}`,
        RegistrationCountryGrptCode: `0098`,
        RegistrationCountryGrptName: "Iran",
        RegistrationCityGrptCode: "021",
        RegistrationCityGrptName: "Tehran",
        EnglishName: CompanyName,
        OfficialNewspaperNo: faker.integer({ min: 1000, max: 9999 }),
        RegisteredCapital: faker.integer({ min: 1000000, max: 10000000000 }),
        AuditorName: randomName(AuditorName),
        AuditorAddress: faker.address(),
        ActivityField: faker.username(),
        WebSite: faker.domain(),
        EmailAddress: faker.email(),
        OrgUnitCode: faker.integer({ min: 1000, max: 9999 }),
        OrgUnitName: faker.username(),
        EconomyCode: faker.integer({ min: 100000000000, max: 999999999999 }),
        BourseCode: faker.integer({ min: 10000, max: 99999 }),
        ArchiveNo: faker.integer({ min: 100, max: 1000 }),
        CompanyDuration: faker.integer({ min: 365, mix: 3650 }),
        LastChangesDate: `${faker.integer({ min: 1300, max: 1398 })}/${faker.integer({ min: 1, max: 12 })}/${faker.integer({ min: 1, max: 29 })}`,
        OwnerShipTypeCode: faker.integer({ min: 1, max: 5 }),
        OwnerShipTypeName: faker.username(),
        ActivityTypeCode: faker.integer({ min: 1, max: 5 }),
        ActivityTypeName: faker.username(),
        CreditCode: faker.integer({ min: 1000, max: 9999 }),
        ExistValidDocuments: faker.pickone(['Y', 'N']),
        IsMoralInfoValid: faker.pickone(['Y', 'N']),
        ShahabCode: `100000${+createValidNationalID() - 1}`,
        ActivityNature: faker.pickone(['F', 'N']),
        FinOrgType: faker.paragraph(),
        EmployeeNumber: faker.pickone(["L100", "M100"]),
        IDExpiryDate: `${faker.integer({ min: 1400, max: 1420 })}/${faker.integer({ min: 1, max: 12 })}/${faker.integer({ min: 1, max: 29 })}`,
        StockHolders: generateStockHolder(faker.integer({ min: 3, max: 7 }), faker),
        Managers: generateManager(faker.integer({ min: 3, max: 7 }), faker),
        Signators: generateSignators(faker.integer({ min: 3, max: 7 }), faker),
        Addresses: generateAddresses(faker.integer({ min: 3, max: 7 }), faker),
    }
})


Factory.blueprint('App/Models/FakeNaturalEntity', (faker, i, data) => {
    let Nationality = faker.pickone(['I', 'F'])
    let NationalId = Nationality === "F" ? undefined : createValidNationalID()
    let ForeignIdentityNo = Nationality === "F" ? createValidNationalID() : undefined
    let name = faker.pickone(Object.keys(randomName()))
    let family = faker.pickone(Object.keys(randomFamilyName()))
    return {
        CustomerCode: faker.integer({ min: 1000, max: 9999 }),
        Nationality: Nationality,
        NationalId: NationalId,
        PassportNo: createValidNationalID(),
        ForeignIdentityNo: ForeignIdentityNo,
        Name: randomName(name),
        Family: randomFamilyName(family),
        PreviousName: randomName(name),
        PreviousFamily: randomFamilyName(family),
        EnglishName: name,
        EnglishFamily: family,
        BirthDate: `${faker.integer({ min: 1300, max: 1388 })}/${faker.integer({ min: 1, max: 12 })}/${faker.integer({ min: 1, max: 29 })}`,
        IsReplica: faker.pickone(['Y', 'N']),
        IdNo: faker.integer({ min: 1, max: 9999999 }),
        IdSeries: faker.pickone(['الف', 'ب']),
        IdSerialNo: faker.integer({ min: 1, max: 9999999 }),
        IdDescription: faker.paragraph(),
        IdIssueOffice: faker.pickone(Object.values(randomProvince())),
        IdIssueDate: `${faker.integer({ min: 1300, max: 1388 })}/${faker.integer({ min: 1, max: 12 })}/${faker.integer({ min: 1, max: 29 })}`,
        FatherName: faker.pickone(Object.values(randomName())),
        BirthCityGrptCode: faker.pickone(Object.keys(randomProvince())),
        BirthPlace: faker.pickone(Object.values(randomProvince())),
        CityGrptCode: faker.pickone(Object.keys(randomProvince())),
        CityGrptName: faker.pickone(Object.values(randomProvince())),
        MaritalStatus: faker.pickone(['M', 'S']),
        NumOfChildren: faker.integer({ min: 0, max: 12 }),
        Gender: faker.pickone(['M', 'F']),
        ResidencyStatus: faker.pickone(['R', 'N']),
        ResidencyExpirationDate: `${faker.integer({ min: 1400, max: 1428 })}/${faker.integer({ min: 1, max: 12 })}/${faker.integer({ min: 1, max: 29 })}`,
        CntrGrptCode: `US`,
        CntrGrptName: `آمریکا`,
        PassportIssueDate: `${faker.integer({ min: 1380, max: 1398 })}/${faker.integer({ min: 1, max: 12 })}/${faker.integer({ min: 1, max: 29 })}`,
        EmailAddress: faker.email(),
        MobileNumber: `0912${faker.integer({ min: 1000000, max: 9999999 })}`,
        JobGroupCode: "1",
        JobGroupName: "Karmand",
        JobTypeCode: "1",
        JobTypeName: "Karmand",
        JobDesc: faker.paragraph(),
        EducationCode: "1",
        EducationName: "Karmand",
        HasGrowthCertificate: faker.pickone(['Y', 'N']),
        CertificateDate: `${faker.integer({ min: 1380, max: 1398 })}/${faker.integer({ min: 1, max: 12 })}/${faker.integer({ min: 1, max: 29 })}`,
        CertificateNumber: faker.integer({ min: 1000, max: 9999 }),
        Issuer: "بانک",
        OrgUnitCode: "1",
        OrgUnitName: "قلهک",
        ExistValidDocuments: faker.pickone(['Y', 'N']),
        ConfirmationStatus: "s",
        EmployeeCode: "1234",
        EconomyCode: faker.integer({ min: 100000000000, max: 999999999999 }),
        BourseCode: `${randomFamilyName(family).slice(0, 3)}${faker.integer({ min: 10000, max: 99999 })}`,
        ActivityTypeCode: "1",
        ActivityTypeName: "قلهک",
        CreditCode: faker.integer({ min: 100000000000, max: 999999999999 }),
        ArchiveNumber: faker.integer({ min: 100000000000, max: 999999999999 }),
        PersonalImage: faker.url(),
        InqueryImage: faker.url(),
        LifeStatus: "Y",
        IncapableCustomer: faker.pickone(['Y', 'N']),
        IncapabilityExpDate: `${faker.integer({ min: 1399, max: 1420 })}/${faker.integer({ min: 1, max: 12 })}/${faker.integer({ min: 1, max: 29 })}`,
        ShahabCode: `100000${NationalId === undefined ? +ForeignIdentityNo - 1 : +NationalId - 1}`,
        IdCardType: "Y",
        IdCardTypeDesc: "Y",
        Addresses: faker.address(),
    }
})
