const { hooks } = require('@adonisjs/ignitor')

hooks.after.providersBooted(() => {
  const Response = use('Adonis/Src/Response')
  Response.macro('sendResponse', function (status, result) {
    this.status(status).send(status)
    this.status(status).json({
      status: status,
      result: result
    })
  })
  Response.macro('sendError', function (status, error) {
    this.status(status).send(status)
    error = Array.isArray(error) ? error : [error]
    this.status(status).json({
      status: status,
      errors: error
    })
  })

});
