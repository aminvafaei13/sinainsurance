"use strict"

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

const Antl = use("Antl")
const Config = use("Config")


Route.group(() => {
  Route.post("/soap/imenta", "SoapController.sendImentaSoap")
    .validator('imenta/sendImentaSoap')
}).prefix('api/v1').formats(['json'])


/**
 * Handle all requests that does not match any of the defined routes
 */
Route.any('*', ({ response }) => {
  return response.sendError(Config.get('base.HttpResponse.not_found'), [
    Antl.formatMessage('routes.not_found')
  ])
})