#!/bin/sh
set -e

 

if [ "$1" == "rest" ]
then  
    exec pm2-runtime start process.json
else
    exec $@
fi
